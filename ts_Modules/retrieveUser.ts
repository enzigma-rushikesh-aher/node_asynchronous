import Promise from 'promise';
import * as fs from 'fs';
import { user } from '../model/user';
import path from 'path';


export function retrieveUser(username:string) {
    return new Promise(function(resolve,reject){
        let filePath = path.join(__dirname,'../')+'userDetails.json';
        const fileData = fs.readFileSync(filePath,'utf-8');
        let arrayOfObjects = JSON.parse(fileData);
        let detail:user;
        let i;
        
        for( i =0;i<arrayOfObjects.userData.length;i++){
           if(arrayOfObjects.userData[i].username == username){
            detail = arrayOfObjects.userData[i];
            resolve(detail);
            break;
           } 
        }   
        if(i == arrayOfObjects.userData.length){
            resolve('No data available');
        }
    })
}