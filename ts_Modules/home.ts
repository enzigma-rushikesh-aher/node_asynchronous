import { question } from 'readline-sync';
import { user } from '../model/user';
import { newUser } from './register';
import { retrieveUser } from './retrieveUser';
import { getLocation } from './googleapi';

function main(){
    const option:string = question('press 1 : for new registration \n press 2 : retrieve details \n press 3 : to call api \n press 4 : Exit ');

    switch(option){
        case '1':
            return newRegistration();
        case '2':
            return retrieveUserData();
        case '3':
            return getLocationOfAddress();
        default:
            return;
    }
}

export function newRegistration(){
    const userName:string = question('Enter Username :');
    const password:string = question('Enter password :');
    const firstName:string = question('Enter FirstName :');
    const lastName:string = question('Enter LastName :');
    const address:string = question('Enter address :');
    let userData:user = <user>{};
    userData.username = userName;
    userData.password = password;
    
    if(firstName.length>0){
        userData.firstName = firstName;
    }
    if(firstName.length>0){
        userData.lastName = lastName;
    }
    if(firstName.length>0){
        userData.address = address;
    }

    newUser(userData).then(function(result){
        console.log(result);
    })

}

function retrieveUserData(){
    const userName:string = question('Enter Username to Search :');

    retrieveUser(userName).then(function(result){
        console.log(result);
    });

}

function getLocationOfAddress(){
    const address:string = question('Enter Address :');
     getLocation(address).then((result)=>{
        console.log(result);
     });
    
}

main();

