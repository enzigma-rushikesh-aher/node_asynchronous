import Promise, { resolve } from 'promise';
import { user } from '../model/user'; 
import * as fs from 'fs';
import path from 'path';

export function newUser(userObject : any){
    return new Promise(function(resolve,reject){
        try{
            let filePath = path.join(__dirname,'../')+'userDetails.json';
            const fileData = fs.readFileSync(filePath,'utf-8');
        let arrayOfObjects = JSON.parse(fileData);
        arrayOfObjects.userData.push(userObject);

        fs.writeFileSync(filePath,JSON.stringify(arrayOfObjects))
        resolve(userObject);
        }catch(err) {
            reject(console.error(err))
        }   
    })
}