import { expect } from 'chai';
import { newUser } from '../ts_Modules/register';
import { user } from '../model/user';
import * as fs from 'fs';


describe('new registration',()=>{
    it('save user',async ()=>{
      let userData:user = { username: "Rushikesh", password: "ar123" };
        let result:any = await newUser(userData);
        console.log(result);
        expect(result.username).to.equal('Rushikesh');
        expect(result.password).to.equal('ar123');
    })
})